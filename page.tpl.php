<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?></script>
  <!--[if lt IE 7]>
    <script src="/ie7/ie7-standard-p.js" type="text/javascript"></script>
  <![endif]-->
</head>

<body>

<div id="header">
    <div id="logo">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if (($site_name) && !($logo)) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </div>
    <div id="menu">
      <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
      <?php print $search_box ?>
    </div>
    <div><?php print $header ?></div>
</div>
<?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>
<div id="wrapper_extra">
<div id="wrapper">

<!-- Block2:  the main content area -->
<div id="block_1">
	<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
  <div id="main">
    <?php print $breadcrumb ?>
    <h1 class="title"><?php print $title ?></h1>
    <div class="tabs"><?php print $tabs ?></div>
    <?php print $help ?>
    <?php print $messages ?>
    <?php print $content; ?>
  </div>
<div class="verticalalign"></div>
</div>
      
<!-- Block2:  the left sidebar -->
<div id="block_2">
    <?php if ($sidebar_left) { ?><div id="sidebar-left">
      <?php print $sidebar_left ?>
    </div><?php } ?>
<div class="verticalalign"></div>
</div>

<!-- Block3:  the right sidebar -->
<div id="block_3">
  <?php if ($sidebar_right) { ?><div id="sidebar-right">
    <?php print $sidebar_right ?>
  </div><?php } ?>
<div class="verticalalign"></div>
</div>    


</div>
</div>

<div id="footer">
  <?php print $footer_message ?>
  "Fern" theme by <a href="http://handelaar.org" title="Designer">John Handelaar</a>, May 2006
</div>
<?php print $closure ?>
</body>
</html>
